{
  description = "Nix-go for VHS";

  # Nixpkgs / NixOS version to use.
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs { inherit system; };
      inherit (pkgs) lib;

      dependencies = with pkgs; [
        ffmpeg
        ttyd
        chromium
      ];

      # to work with older version of flakes
      lastModifiedDate = self.lastModifiedDate or self.lastModified or "19700101";

      # Generate a user-friendly version number.
      version = builtins.substring 0 8 lastModifiedDate;

      vhs-exec = pkgs.buildGoModule {
        pname = "vhs";
        buildInputs = dependencies;

        inherit version;
        # In 'nix develop', we don't need a copy of the source tree
        # in the Nix store.
        src = ./.;

        # This hash locks the dependencies of this package. It is
        # necessary because of how Go requires network access to resolve
        # VCS.  See https://www.tweag.io/blog/2021-03-04-gomod2nix/ for
        # details. Normally one can build with a fake sha256 and rely on native Go
        # mechanisms to tell you what the hash should be or determine what
        # it should be "out-of-band" with other tooling (eg. gomod2nix).
        # To begin with it is recommended to set this, but one must
        # remeber to bump this hash when your dependencies change.
        #vendorSha256 = pkgs.lib.fakeSha256;

        vendorSha256 = "sha256-9nkRr5Jh1nbI+XXbPj9KB0ZbLybv5JUVovpB311fO38=";
      }; 
    in
    {
      packages.default = pkgs.symlinkJoin {
        # Wrap vhs-exec to provide dependencies on the path
        # See https://ertt.ca/nix/shell-scripts/
        name = "vhs";
        paths = [ vhs-exec ] ++ dependencies;
        buildInputs = [ pkgs.makeWrapper ];
        postBuild = "wrapProgram $out/bin/vhs --prefix PATH : $out/bin";
      };

      devShell = pkgs.mkShell {
        #buildInputs = dependencies;
      };
    });
}
